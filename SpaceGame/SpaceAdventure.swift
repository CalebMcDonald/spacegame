//
//  SpaceAdventure.swift
//  SpaceGame
//
//  Created by Caleb McDonald on 31/07/2016.
//  Copyright © 2016 Caleb McDonald. All rights reserved.
//

import Foundation

class SpaceAdventure {
    
    let planetarySystem:PlanetarySystem
    
    init(planetarySystem:PlanetarySystem){
        self.planetarySystem = planetarySystem
    }
    private func displayIntroduction (){
        print("Welcome to the \(planetarySystem.name)!")
        print("There are \(planetarySystem.planets.count) planets to explorer.")
    }
    
    private func responseToPrompt (prompt:String) -> String {
        print(prompt)
        return getUserInput()
    }
    
    private func greetAdevnturer(){
        let name = responseToPrompt("What is your name?")
        print("Nice to meet you, \(name). My name is Hiro, I'm an old friend of Siri.")
    }
    
    private func visit (planetName:String){
        print("Traveling to \(planetName)...")
        
        for planet in planetarySystem.planets {
            if planetName == planet.name{
                print("You've now arrived at \(planet.name). \(planet.description)")
            }
            
        }
    }
    private func determineDestination() {
        var decision = ""
        
        while !(decision == "Y" || decision == "N") {
            decision = responseToPrompt("Shall I randomly choose a planet for you to visit? (Y or N)")
            
            if decision == "Y" {
                if let planet = planetarySystem.randomPlanet {
                    visit(planet.name)
                } else {
                    print("Sorry, but there are no planets in this system.")
                }
            } else if decision == "N" {
                let planetName = responseToPrompt("OK, name the planet you would like to visit?")
                visit(planetName)
            } else {
                print("Sorry, I didn't get that.")
            }
        }
        
    }
    func start(){
        displayIntroduction()
        greetAdevnturer()
        if !planetarySystem.planets.isEmpty {
            print("Let's go on an adventure!")
            determineDestination()
        }
    }
}
