//
//  PlanetarySystem.swift
//  SpaceGame
//
//  Created by Caleb McDonald on 31/07/2016.
//  Copyright © 2016 Caleb McDonald. All rights reserved.
//

import Foundation

class PlanetarySystem {
    
    let name:String
    let planets:[Planet]
    
    var randomPlanet:Planet? {
        if planets.isEmpty {
            return nil
        } else {
            let index = Int(arc4random_uniform(UInt32(planets.count)))
            return planets[index]
        }
    }
    
    init(name:String, planets:[Planet]){
        self.name = name
        self.planets = planets
    }
    
}