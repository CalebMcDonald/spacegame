//
//  Planet.swift
//  SpaceGame
//
//  Created by Caleb McDonald on 31/07/2016.
//  Copyright © 2016 Caleb McDonald. All rights reserved.
//

import Foundation

class Planet {
    
    let name:String
    let description:String
    
    init (name:String, description:String) {
        self.name = name
        self.description = description
        
    }
}