//
//  Helper.swift
//  SpaceGame
//
//  Created by Caleb McDonald on 31/07/2016.
//  Copyright © 2016 Caleb McDonald. All rights reserved.
//

import Foundation

func getUserInput () -> String {
    
    let standardInput = NSFileHandle.fileHandleWithStandardInput()
    var input = NSString(data: standardInput.availableData, encoding: NSUTF8StringEncoding)
    
    input = input!.stringByTrimmingCharactersInSet(NSCharacterSet.newlineCharacterSet())
    
    return input as! String
}